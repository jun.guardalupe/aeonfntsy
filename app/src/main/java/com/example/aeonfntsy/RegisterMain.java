package com.example.aeonfntsy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import butterknife.BindView;

public class RegisterMain extends AppCompatActivity {

    Spinner spnrBranch;
    LinearLayout llNextBtn;

    String[] branchLst = { "Makati", "Taguig", "Quezon", "Ortigas" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_main);

        spnrBranch = findViewById(R.id.spnrBranch);
        llNextBtn = findViewById(R.id.llNextBtn);

        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, branchLst);
        spnrBranch.setAdapter(adapter);

        llNextBtn.setOnClickListener(v -> {
            Intent intent = new Intent(RegisterMain.this, RegisterChildren.class);
            startActivity(intent);
        });
    }
}