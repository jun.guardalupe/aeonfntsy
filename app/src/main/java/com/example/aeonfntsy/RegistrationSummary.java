package com.example.aeonfntsy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.aeonfntsy.Enums.Gender;
import com.example.aeonfntsy.adapters.ChildrenAdapter;
import com.example.aeonfntsy.adapters.RegSummaryAdapter;
import com.example.aeonfntsy.models.ChildrenModel;

import java.util.ArrayList;
import java.util.List;

public class RegistrationSummary extends AppCompatActivity {

    LinearLayout llNextBtn;
    ImageView imgKidzoona;
    RecyclerView rvChildren;
    RecyclerView.Adapter adapter;
    private List<ChildrenModel> childrens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_summary);

        llNextBtn = findViewById(R.id.llNextBtn);
        imgKidzoona = findViewById(R.id.imgKidzoona);
        rvChildren = findViewById(R.id.rvChildren);
        rvChildren.setHasFixedSize(true);

        childrens = new ArrayList<>();
        childrens.add(new ChildrenModel("Michiko", "10", Gender.MALE));
        childrens.add(new ChildrenModel("Kenji", "6", Gender.MALE));
        childrens.add(new ChildrenModel("Kathy", "8", Gender.MALE));
        childrens.add(new ChildrenModel("Fatima", "8", Gender.MALE));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new RegSummaryAdapter(childrens, this);
        rvChildren.setLayoutManager(layoutManager);
        rvChildren.setAdapter(adapter);

        SetHeaderImgHeight();
    }

    private void SetHeaderImgHeight(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        float heightVsWidth = 266f / 833f;
        float imgHeight = width * heightVsWidth;
        imgKidzoona.getLayoutParams().height = (int)imgHeight;
        imgKidzoona.requestLayout();
    }
}