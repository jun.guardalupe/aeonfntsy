package com.example.aeonfntsy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.aeonfntsy.Enums.Gender;
import com.example.aeonfntsy.adapters.ChildrenAdapter;
import com.example.aeonfntsy.models.ChildrenModel;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class RegisterChildren extends AppCompatActivity {

    private List<ChildrenModel> childrens;
    private RecyclerView childRec;
    RecyclerView.Adapter adapter;

    LinearLayout llNextBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_children);

        childRec = findViewById(R.id.childRec);
        SetupArrayAdapter();

        LinearLayout llAddBtn = findViewById(R.id.llAddBtn);
        llAddBtn.setOnClickListener(v -> {
            childrens.add(new ChildrenModel("", "", Gender.MALE));
            adapter.notifyDataSetChanged();
        });

        llNextBtn = findViewById(R.id.llNextBtn);
        llNextBtn.setOnClickListener(v -> {
            Intent intent = new Intent(RegisterChildren.this, RegistrationSummary.class);
            startActivity(intent);
        });
    }

    private void SetupArrayAdapter(){
        childrens = new ArrayList<>();
        childrens.add(new ChildrenModel("", "", Gender.MALE));
        childrens.add(new ChildrenModel("", "", Gender.MALE));
        childrens.add(new ChildrenModel("", "", Gender.MALE));
        childrens.add(new ChildrenModel("", "", Gender.MALE));
        childRec.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new ChildrenAdapter(childrens, this);
        childRec.setLayoutManager(layoutManager);
        childRec.setAdapter(adapter);
    }
}