package com.example.aeonfntsy;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.aeonfntsy.dialogs.VerificationDialog;

public class VerifyEmail extends AppCompatActivity {

    private ImageView imgFooter;
    private LinearLayout llNextBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_email);

        imgFooter = findViewById(R.id.imgFooter);
        llNextBtn = findViewById(R.id.llNextBtn);

        llNextBtn.setOnClickListener(v -> {
            VerificationDialog dialog = new VerificationDialog(VerifyEmail.this);
            dialog.show();
        });

        computeFooterHeight();
    }

    private void computeFooterHeight(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        // Image height over width
        float heightVsWidth = 416f / 844f;
        float imgHeight = width * heightVsWidth;
        imgFooter.getLayoutParams().height = (int)imgHeight;
        imgFooter.requestLayout();
    }
}