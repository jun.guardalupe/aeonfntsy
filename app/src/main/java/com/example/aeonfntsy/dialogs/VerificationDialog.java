package com.example.aeonfntsy.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.aeonfntsy.R;
import com.example.aeonfntsy.RegisterChildren;
import com.example.aeonfntsy.RegisterMain;
import com.example.aeonfntsy.VerifyEmail;

public class VerificationDialog extends Dialog {

    EditText code1, code2, code3, code4;
    LinearLayout llBtnVerify;
    Context context;

    public VerificationDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.verify_email_dialog);

        code1 = findViewById(R.id.code1);
        code2 = findViewById(R.id.code2);
        code3 = findViewById(R.id.code3);
        code4 = findViewById(R.id.code4);

        llBtnVerify = findViewById(R.id.llBtnVerify);

        llBtnVerify.setOnClickListener(v -> {

        });

    }
}
