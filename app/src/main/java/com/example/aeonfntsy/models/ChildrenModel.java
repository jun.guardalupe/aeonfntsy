package com.example.aeonfntsy.models;

import com.example.aeonfntsy.Enums.Gender;

public class ChildrenModel {

    private String fName;
    private String birthday;
    private Gender gender;

    public ChildrenModel(String fName, String birthday, Gender gender) {
        this.fName = fName;
        this.birthday = birthday;
        this.gender = gender;
    }

    public String getfName() {
        return fName;
    }

    public String getBirthday() {
        return birthday;
    }

    public Gender getGender() {
        return gender;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }
}
