package com.example.aeonfntsy.models;

public class RegistrationModel {

    public String FirstName;
    public String LastName;
    public String Nickname;
    public String ContactNumber;
    public String StoreBranch;
    public String Email;
    public String Password;

    public RegistrationModel(String firstName, String lastName, String nickname, String contactNumber, String storeBranch, String email, String password) {
        FirstName = firstName;
        LastName = lastName;
        Nickname = nickname;
        ContactNumber = contactNumber;
        StoreBranch = storeBranch;
        Email = email;
        Password = password;
    }
}
