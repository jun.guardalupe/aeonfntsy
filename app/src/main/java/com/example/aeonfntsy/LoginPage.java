package com.example.aeonfntsy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class LoginPage extends AppCompatActivity {

    private ImageView imgFooter;
    private LinearLayout llNextBtn;
    private CardView cvRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        imgFooter = findViewById(R.id.imgFooter);
        llNextBtn = findViewById(R.id.llNextBtn);
        cvRegister = findViewById(R.id.cvRegister);

        cvRegister.setOnClickListener(v -> {
            Intent intent = new Intent(LoginPage.this, VerifyEmail.class);
            startActivity(intent);
        });

        computeFooterHeight();
    }

    private void computeFooterHeight(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        // Image height over width
        float heightVsWidth = 334f / 521f;
        float imgHeight = width * heightVsWidth;
        imgFooter.getLayoutParams().height = (int)imgHeight;
        imgFooter.requestLayout();
    }
}