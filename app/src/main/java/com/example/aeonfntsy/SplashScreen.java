package com.example.aeonfntsy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashScreen extends AppCompatActivity {

    @BindView(R.id.imgLoading)
    ImageView imgLoading;

    @BindView(R.id.imgKidzoona)
    ImageView imgKidzoona;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);

        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.clockwise_rotation);
        rotation.setRepeatCount(Animation.INFINITE);
        imgLoading.startAnimation(rotation);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this, LoginPage.class);
                ActivityOptionsCompat option = ActivityOptionsCompat.makeSceneTransitionAnimation(SplashScreen.this, imgKidzoona, ViewCompat.getTransitionName(imgKidzoona));
                startActivity(intent, option.toBundle());
            }
        }, 2500);
    }
}