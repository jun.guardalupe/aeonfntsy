package com.example.aeonfntsy.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;

import androidx.annotation.Nullable;

public class EdittextAutoNext extends androidx.appcompat.widget.AppCompatEditText{

    private static EdittextAutoNext mInstance;
    public static final String XML_NAMESPACE_ANDROID = "http://schemas.android.com/apk/res/android";
    int txtMaxLength;
    int txtCurrentLength;

    public EdittextAutoNext(Context context, AttributeSet attrs) {
        super(context, attrs);
        txtMaxLength = attrs.getAttributeIntValue(XML_NAMESPACE_ANDROID, "maxLength", -1);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        if(txtMaxLength == -1) return;
        View vRight = this.focusSearch(View.FOCUS_RIGHT);
        View vDown = this.focusSearch(View.FOCUS_DOWN);

        if(lengthAfter == txtMaxLength){
            if(vRight != null) vRight.requestFocus();
            else if(vDown != null) vDown.requestFocus();
        }

        txtCurrentLength = lengthAfter;
    }

    /*@Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL) {
            View vLeft = this.focusSearch(View.FOCUS_LEFT);
            View vUp = this.focusSearch(View.FOCUS_UP);
            if(txtCurrentLength == 0){
                if(vLeft != null) vLeft.requestFocus();
                else if(vUp != null) vUp.requestFocus();
            }
        }

        return false;
    }*/

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        return new MyInputConnection(super.onCreateInputConnection(outAttrs), true);
    }

    private class MyInputConnection extends InputConnectionWrapper {

        public MyInputConnection(InputConnection target, boolean mutable) {
            super(target, mutable);
        }

        @Override
        public boolean sendKeyEvent(KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_DOWN
                    && event.getKeyCode() == KeyEvent.KEYCODE_DEL) {

                View vLeft = EdittextAutoNext.this.focusSearch(View.FOCUS_LEFT);
                View vUp = EdittextAutoNext.this.focusSearch(View.FOCUS_UP);
                if(txtCurrentLength == 0){
                    if(vLeft != null) vLeft.requestFocus();
                    else if(vUp != null) vUp.requestFocus();
                }
                // Un-comment if you wish to cancel the backspace:
                // return false;
            }
            return super.sendKeyEvent(event);
        }


        @Override
        public boolean deleteSurroundingText(int beforeLength, int afterLength) {
            // magic: in latest Android, deleteSurroundingText(1, 0) will be called for backspace
            if (beforeLength == 1 && afterLength == 0) {
                // backspace
                return sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL))
                        && sendKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DEL));
            }

            return super.deleteSurroundingText(beforeLength, afterLength);
        }

    }
}
