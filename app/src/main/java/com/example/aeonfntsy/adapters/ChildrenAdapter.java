package com.example.aeonfntsy.adapters;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.aeonfntsy.Enums.Gender;
import com.example.aeonfntsy.R;
import com.example.aeonfntsy.models.ChildrenModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class ChildrenAdapter extends RecyclerView.Adapter<ChildrenAdapter.ChildrenViewHolder> {

    private List<ChildrenModel> childrens;
    private Context context;
    private final Calendar newDate = Calendar.getInstance();
    private final SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy", Locale.US);

    public ChildrenAdapter(List<ChildrenModel> childrens, Context context) {
        this.childrens = childrens;
        this.context = context;
    }

    @Override
    public ChildrenViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.children_item, parent, false);
        ChildrenViewHolder viewHolder = new ChildrenViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChildrenAdapter.ChildrenViewHolder holder, int position) {
        ChildrenModel children = childrens.get(position);
        holder.etFirstName.setText(children.getfName());
        holder.etBirthday.setText(children.getBirthday());

        holder.llSwitch.setOnClickListener(v -> {
            switch (children.getGender()){

                case MALE:
                    holder.llSwitch.setBackgroundResource(R.drawable.ic_gender_girl);
                    children.setGender(Gender.FEMALE);
                    holder.txtBoy.setTextColor(Color.parseColor("#9b9b9b"));
                    holder.txtGirl.setTextColor(Color.parseColor("#FFFFFFFF"));
                    break;
                case FEMALE:
                    holder.llSwitch.setBackgroundResource(R.drawable.ic_gender_boy);
                    children.setGender(Gender.MALE);
                    holder.txtGirl.setTextColor(Color.parseColor("#9b9b9b"));
                    holder.txtBoy.setTextColor(Color.parseColor("#FFFFFFFF"));
                    break;
            }
        });

        holder.mainLayout.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return childrens.size();
    }

    public static class ChildrenViewHolder extends RecyclerView.ViewHolder {

        public EditText etFirstName, etBirthday;
        public LinearLayout llSwitch, mainLayout;
        public TextView txtBoy, txtGirl;

        public ChildrenViewHolder(View itemView) {
            super(itemView);
            etFirstName = itemView.findViewById(R.id.etFirstName);
            etBirthday = itemView.findViewById(R.id.etBirthday);
            llSwitch = itemView.findViewById(R.id.llSwitch);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            txtBoy = itemView.findViewById(R.id.txtBoy);
            txtGirl = itemView.findViewById(R.id.txtGirl);
        }
    }
}
