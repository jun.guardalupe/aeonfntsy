package com.example.aeonfntsy.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.aeonfntsy.R;
import com.example.aeonfntsy.models.ChildrenModel;

import java.util.List;

public class RegSummaryAdapter extends RecyclerView.Adapter<RegSummaryAdapter.SummaryViewHolder> {

    private List<ChildrenModel> childrens;
    private Context context;

    public RegSummaryAdapter(List<ChildrenModel> childrens, Context context) {
        this.childrens = childrens;
        this.context = context;
    }

    @Override
    public SummaryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reg_summary_item, parent, false);
        return new SummaryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RegSummaryAdapter.SummaryViewHolder holder, int position) {
        ChildrenModel children = childrens.get(position);
        holder.etName.setText(children.getfName());
        holder.etAge.setText(children.getBirthday());
        switch (children.getGender()){

            case MALE:
                holder.etGender.setText("Male");
                break;
            case FEMALE:
                holder.etGender.setText("Female");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return childrens.size();
    }

    public static class SummaryViewHolder extends RecyclerView.ViewHolder{
        TextView etName, etAge, etGender;

        public SummaryViewHolder(View itemView) {
            super(itemView);
            etName = itemView.findViewById(R.id.etName);
            etAge = itemView.findViewById(R.id.etAge);
            etGender = itemView.findViewById(R.id.etGender);
        }
    }

}
